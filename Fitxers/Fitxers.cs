﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Fitxers
{
    class Fitxers
    {
        static void Main(string[] args)
        {
            Fitxers app = new Fitxers();
            app.Menu();
        }

        public void Menu()
        {
            string path = @"F:\DAM\M3\UF3\M03Uf3FitxersNavarreteAngel\Fitxers\bin\Debug\netcoreapp3.1";
            MostrarOpcionsMenu();

            string opcio = DemanarOpcioMenu();
            EscollirOpcioMenu(opcio, path);
        }
        public void MostrarOpcionsMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                                         Exercicis Fitxers Angel Navarrete");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("\t1.- ParelliSenar");
            Console.WriteLine("\t2.- Inventari");
            Console.WriteLine("\t3.- Wordcount");
            Console.WriteLine("\t4.- ConfigFileLanguage");
            Console.WriteLine("\t5.- FindPaths");
            Console.WriteLine("\t0.- Sortir");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public string DemanarOpcioMenu()
        {
            Console.Write("Escull una opció: ");
            string opcio = Console.ReadLine();
            return opcio;
        }

        public void EscollirOpcioMenu(string opcio, string path)
        {
            do
            {
                switch (opcio)
                {
                    case "1":
                        ParellSenar(path);
                        break;
                    case "2":
                        Inventari(path);
                        break;
                    case "3":
                        Wordcount(path);
                        break;
                    case "4":
                        ConfigFileLanguage(path);
                        break;
                    case "5":
                        FindPaths();
                        break;
                    case "0":
                        Console.WriteLine("Adeu");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opcio Incorrecta");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
                Console.ReadLine();
                Console.Clear();
                MostrarOpcionsMenu();
                opcio = DemanarOpcioMenu();
            } while (opcio != "0");
        }

        /*DESCRIPTION:  Generar un fichero que contenga sólo los numeros pares y otro fichero para numeros impares (los números han de estar ordenados de mayor a menor)*/
        string ParellSenar(string pathDirectori)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------Parell i Senar--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            string resultat = "";
            string pathFitxer = pathDirectori + "\\Numbers.txt";
            //StreamWriter fitxerPar = new StreamWriter(pathFitxer + "\\par.txt");
            //StreamWriter fitxerImpar = new StreamWriter(pathFitxer + "\\impar.txt");
            File.Delete(pathDirectori + "parell.txt");
            File.Delete(pathDirectori + "sencer.txt");

            if (File.Exists(pathFitxer))
            {

                StreamReader sr = File.OpenText(pathFitxer);
                List<string> liniesNumbers = new List<string>();
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    liniesNumbers.Add(line);
                    //Console.WriteLine(line);
                }
                sr.Close();
                liniesNumbers.Reverse();

                for (int i = 0; i < liniesNumbers.Count; i++)
                {
                    if (IsParell(liniesNumbers[i]))
                    {
                        resultat = "parell.txt";
                        EscribirNumero(pathDirectori, resultat, liniesNumbers[i]);
                    }
                    else
                    {
                        resultat = "senar.txt";
                        EscribirNumero(pathDirectori, resultat, liniesNumbers[i]);
                    }
                }
                Console.WriteLine("Pares creados correctamente en el fichero" + Path.GetFullPath("parell.txt"));
                Console.WriteLine("Impares creados correctamente en el fichero"+ Path.GetFullPath("senar.txt"));

            }
            else
            {
                Console.WriteLine("El Fitxer no existeix");
            }
            return resultat;
            
        }
        bool IsParell(string num)
        {
            if (Convert.ToInt32(num)%2==0){
                return true;
            }
            else
            {
                return false;
            }
        }
        void EscribirNumero(string pathDirectori, string fitxer, string numero) { 
            string fitxerFinal = pathDirectori +fitxer;
            StreamWriter fitxerAEscriure ;

            if (!File.Exists(fitxerFinal))
            {
                fitxerAEscriure = new StreamWriter(fitxerFinal);
            }
            else
            {
                fitxerAEscriure = File.AppendText(fitxer);
            }
            fitxerAEscriure.WriteLine(numero);
            fitxerAEscriure.Close();

        }

        /*DESCRIPTION: Crea un programa para gestionar un inventario, hazlo persistente guardando los datos en un fichero txt. Separa los campos utilizando una coma.*/
        void Inventari(string path )
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------Inventari--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            string element= "";

            Console.Write("Introdueix el nom: ");
            element += Console.ReadLine() + ", ";
            Console.Write("Introdueix el model: ");
            element += Console.ReadLine() + ", ";
            Console.Write("Introdueix el MAC: ");
            element += Console.ReadLine() + ", ";
            Console.Write("Introdueix l'any de fabricació: ");
            element += Console.ReadLine();

            InsertaElementEnFitxer(path, element,"Inventari.txt");
            MostrarElementsInventari(path);
        }
        void InsertaElementEnFitxer(string path, string element, string nomFitxer)
        {
            string fitxerFinal = path + "\\"+nomFitxer;
            StreamWriter fitxerAEscriure;

            if (File.Exists(fitxerFinal)==false)
            {
                fitxerAEscriure = new StreamWriter(fitxerFinal);
            }
            else
            {
                fitxerAEscriure = File.AppendText(fitxerFinal);
                fitxerAEscriure.WriteLine(element);
            }
            
            fitxerAEscriure.Close();
        }
        void MostrarElementsInventari(string path)
        {

            if (File.Exists(path + "\\Inventari.txt"))
            {
                StreamReader sr = File.OpenText(path + "\\Inventari.txt");
                Console.WriteLine(sr.ReadToEnd());
                sr.Close();
            }
            else
            {
                Console.WriteLine("El fitxer no existeix");
            }
            

        }
        /*DESCRIPTION: Contar apariciones de una palabra en un fichero de texto wordcount.txt*/
        void Wordcount(string path)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------WordCount--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            string palabra;
            do
            {
                Console.Write("> ");
                palabra = Console.ReadLine();
                if (palabra != "END")
                {
                    ContarLletres(path, palabra, "wordcount.txt");
                }
            } while (!palabra.Contains("END"));
        }

        private void ContarLletres(string path, string palabra, string nomFitxer)
        {
            if (File.Exists(path + "\\" + nomFitxer))
            {
                StreamReader sr = File.OpenText(path + "\\"+ nomFitxer);
                char[] separators = { ' ', '.', ',', ';' };
                string[] palabras = palabra.Split(" ");
                string[] palabrasWordCount = (sr.ReadToEnd()).Split(separators);
                int contador = 0;
                for (int i = 0; i < palabras.Length; i++)
                {
                    foreach (string palabraWordCount in palabrasWordCount)
                    {
                        if(palabraWordCount.ToUpper().Contains(palabras[i].ToUpper()))
                        {
                            contador++;
                        }
                    }
                    Console.WriteLine(palabras[i] + ": " + contador);
                    contador = 0;
                }
                sr.Close();
            }
            else
            {
                Console.WriteLine("El fitxer no existeix");
            }
            
        }
        /*DESCRIPTION: Crea un programa que utilice los valores del fichero config.txt y permita seleccionar el idioma en el que nos comunicaremos con el usuario*/
        void ConfigFileLanguage(string path)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------ConfigFileLanguage--------------------");
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Intorduce tu nombre: ");
            StreamReader sr = File.OpenText(path + "\\config.txt");
            string[] palabrasWordCount = (sr.ReadLine()).Split(":");
            palabrasWordCount[1] = Console.ReadLine();
            string nomUsuari = palabrasWordCount[1];

            Console.Write("> ");
            string idioma = Console.ReadLine();
            string[] idiomaSplit = idioma.Split(" ");

            if (idiomaSplit[1].ToLower().Equals("es"))
            {
                LlegirFitxer(path, nomUsuari,"lang\\lng_es.txt");
            }
            else if (idiomaSplit[1].ToLower().Equals("cat"))
            {
                LlegirFitxer(path, nomUsuari, "lang\\lng_cat.txt");
            }
            else
            {
                LlegirFitxer(path, nomUsuari, "lang\\lng_es.txt");
            }
            
        }

        private void LlegirFitxer(string path, string nomUsuari,string nomFitxer)
        {
            if (File.Exists(path + "\\"+ nomFitxer))
            {
                StreamReader sr = File.OpenText(path +"\\"+ nomFitxer);
                Console.WriteLine(sr.ReadToEnd() + nomUsuari);
                sr.Close();
            }
            else
            {
                Console.WriteLine("El fitxer no existeix");
            }
        }

        /*DESCRIPTION: L’objectiu d’aquesta activitat és saber recórrer una jerarquia de carpetes i fitxers per realitzar operacions sobre ella.*/
        void FindPaths()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------ConfigFileLanguage--------------------");
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("> Quin és el nom del fitxer a cercar? ");
            string fitxer = Console.ReadLine();
            Console.WriteLine(">Escriu el nom d'una ruta a una carpeta:");
            string path = Console.ReadLine();

            BuscarFitxer(path, fitxer);
        }

        void BuscarFitxer(string path, string fitxer)
        {
            string[] extension = fitxer.Split('.');
            try
            {
                foreach (string files in Directory.GetFiles(path + "\\", "*." + extension[1]))
                {
                    if (files.Equals(path + "\\" + fitxer))
                    {
                        Console.WriteLine("S'ha trobat el fitxer a: " + Path.GetFullPath(files));

                    }
                }
                foreach (string directoris in Directory.GetDirectories(path))
                {
                    BuscarFitxer(directoris, fitxer);
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
